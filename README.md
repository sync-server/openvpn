### OpenVPN in docker-compose

[I use this VPS hosting](https://www.hostens.com/?affid=1251)

[Reference](https://habr.com/ru/post/354632/)

## Run openvpn using docker-compose
1. Clone this repo and cd into it
    ```bash
    git clone git@gitlab.com:sync-server/openvpn.git && \
    cd openvpn
    ```

1. Get your external IP:
    ```bash
    HOST_ADDR=$(curl -s https://api.ipify.org)
    ```

1. Initilization of openvpn
    ```bash
    docker-compose run --rm openvpn ovpn_genconfig -u udp://${HOST_ADDR}
    ```

1. Creat certificate
    ```bash
    docker-compose run --rm openvpn ovpn_initpki
    ```
    - Enter pass phrase, write it down
    - Enter your name of VPN (for examle name of your host mashine)

1. Up service
    ```bash
    docker-compose up -d
    ```

1. Create client certificate for connecting to openvpn.

    Change value of `CLIENT_NAME`
    ```bash
    CLIENT_NAME=laptop
    docker-compose run --rm openvpn easyrsa build-client-full $CLIENT_NAME nopass
    ```

1. Export certificate. Use this certificate to connect openvpn.
    ```bash
    docker-compose run --rm openvpn ovpn_getclient $CLIENT_NAME > vpn-$CLIENT_NAME.ovpn
    ```

1. Repeat previous two steps for another client if needed.

## In case of different port for VPN

In case you forward on router another port for VPN, not `1194`

You need to change external port in ovpn file:
    
Example:

```bash
sed -i "/^remote/s/1194/21194/" vpn-$CLIENT_NAME.ovpn
```

Where `21194` is your external port for VPN
